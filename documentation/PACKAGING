INTRODUCTION
~~~~~~~~~~~~

This document attempts to establish guidelines for people making binary
packages of Wine.

It expresses the basic principles that the Wine developers have agreed
should be used when building Wine. It also attempts to highlight the areas
where there are different approaches to packaging Wine, so that the packager
can understand the different alternatives that have been considered and their
rationales.

TERMS
~~~~~

There are several terms and paths used in this document as place holders
for configurable values. Those terms are described here.
    * WINEPREFIX: is the user's Wine configuration directory.
	This is almost always ~/.wine, but can be overridden by
        the user by setting the WINEPREFIX environment variable.

    * PREFIX: is the prefix used when selecting an installation target.
	The current default is /usr/local. This results in binary
	installation into /usr/local/bin,  library installation into
	/usr/local/wine/lib, and so forth.
        This value can be overridden by the packager. In fact, FHS 2.2
	(http://www.pathname.com/fhs/) specifications suggest that a better
        prefix is /opt/wine.  Ideally, a packager would also allow the
	installer to override this value.

    * WINDOWSDIR: is an important concept to Wine.  This directory specifies
	what directory corresponds to the root Windows directory
        (e.g. C:\WINDOWS). This directory is specified by the user, in
        their registry settings.  Generally speaking, this directory
	is either set to point at an empty directory, or it is set to point
	at a Windows partition that has been mounted through the vfat driver.
        NOTE:   It is extremely important that the packager understand the
		importance of WINDOWSDIR and convey this information and
		choice to the end user.

DEPENDENCIES
~~~~~~~~~~~~

There are two types of dependencies: hard and soft dependencies.

A hard dependency must be available at runtime for Wine to function,
if  compiled into the code. Soft dependencies on the other hand
will degrade gracefully at runtime if unavailable on the runtime system.
Ideally, we should eliminate all hard dependencies in favor of
soft dependencies.

To enable a soft dependency, it must be available at compile time.
As a packager, please do your best to make sure that as many soft
dependencies  are available during compilation. Failing to have a
soft dependency available means that users cannot benefit
from a Wine capability.

Here is a list of the soft dependencies. We suggest packagers
install each and every last of those before building the package.
These libraries are not dependencies in the RPM sense. In DEB packages,
they should appear as "Suggests" or "Recommends", as the case may be.

    * libresolv
      This is the standard Unix domain name resolver library that is
      used to implement DNS support found in dnsapi.dll. resolv.h as
      well as libresolv.so should be present at build time.

    * FreeType: http://www.freetype.org
      This library is used for direct rendering of fonts. It provides
      better support of fonts than using the X11 fonts engine. It is
      only needed for the X11 back end engine. Used from GDI.

    * fontforge: http://sourceforge.net/projects/fontforge/
      Used by WINE to create our own set of TrueType fonts during build.

    * fontconfig
      Used to find TrueType fonts for rendering with freetype. Used by
      GDI.

    * Alsa: http://sourceforge.net/projects/alsa (Linux only)
      This library gives sound support to the Windows environment.

    * JACK: http://jackit.sourceforge.net
      Similar to Alsa, it allow Wine to use the JACK audio server.

    * CUPS: http://www.cups.org
      This library allows Windows to see CUPS defined printers. Used
      by WINEPS and WINSPOOL.

    * OpenGL
      This is used for both OpenGL and Direct3D (and some other
      DirectX functions as well) support in Wine. There are many many
      libraries for providing this functionality. It is enough for one
      of them to be available when compiling Wine. Wine can work with
      any other library during runtime.
      If no library is available, packagers are encouraged to compile
      Wine with Mesa3D (http://www.mesa3d.org), which requires no
      hardware support to install.

    * OpenLDAP: http://www.openldap.org
      Used by WLDAP32 to implement LDAP support.

    * LittleCMS: http://www.littlecms.com
      This library is used to implement MSCMS (Color Management System)
      which is needed by an increasing number of graphics applications.

    * libjpeg
      This library is used to load JPEG files within OLE automation.

    * libungif or gif_lib
      One of these two libraries is used to load GIF files within OLE
      automation.

    * ICU
      Used for bidirectional character output. Linked statically, used
      by GDI.

    * sane
      Used for basic scanner support in our TWAIN32 library.

    * openssl
      Used for some cryptographic support in ADVAPI32.

    * Xrandr, Xrender, Xi, Xext
      X11 extension libraries used by the x11drv.
      Xrandr - resolution switching
      Xrender - client side font rendering
      Xi - X Input handling (for asian input methods mostly)
      Xext - X extensions

    * libxml2, libxslt
      These two libraries are used for our msxml3.dll implementation.

    * prelink
      Used during build to locate WINE at a specific virtual address.

    * capi4linux
      Used to implement our capi2032.dll.

    * libgphoto2
      Used to implement our gphoto2.ds (TWAIN datasource)

    * hal, dbus-1
      Used to implement CD mount handling (via explorer.exe).

GOALS
~~~~~

An installation from a Wine package should:
    * Install quickly and simply:
	The initial installation should require no user input.  An
		'rpm -i wine.rpm' or 'apt-get install wine'
        should suffice for initial installation.

    * Work quickly and simply:
        The user should be able to launch Solitaire within seconds
	of downloading the Wine package.

    * Comply with File system Hierarchy Standard
        A Wine installation should, as much as possible, comply
        with the FHS standard (http://www.pathname.com/fhs/).

    * Preserve flexibility
        None of the flexibility built into Wine should
        be hidden from the end user.

    * Easy configuration
        Come as preconfigured as possible, so the user does
        not need to change any configuration files.

    * Small footprint
        Use only as much disk space as needed per user.

    * Reduce support requirements.
        A packaged version of Wine should be sufficiently easy to use and
	have quick and easy access to FAQs and documentation such that
	requests to the newsgroup and development group go down.
        Further, it should be easy for users to capture good bug reports.

REQUIREMENTS
~~~~~~~~~~~~

Successfully installing Wine requires:

    * Install of the .rpm or .deb package.

    * No longer: Preparing a fake windows setup.

	If WINEPREFIX is not present, wine will generate a setup
	by itself by calling wineprefixcreate.

        This will load all default registry entries, and register dlls
	where necessary. A special "wine.inf" file is provided with
	the WINE sources and installed to /usr/share/wine/.

WINE COMPONENTS
~~~~~~~~~~~~~~~

   * Executable Files
	- cmd  : Wine's command line interpreter, a cmd.exe replacement.
	- notepad : The windows Notepad replacement.
	- progman : A Program Manager replacement.
	- regedit : A graphical tool to edit your registry or for
		importing a windows registry to Wine.
	- regsvr32 : A program to register/unregister .DLL and .OCX files.
		Only works on those dlls that can self-register.
	- taskmgr : A clone of the windows taskmgr, used for debugging and
		managing running Windows and Winlib processes.
	- uninstaller: A program to uninstall installed Windows programs.
		Like the Add/Remove Program in the windows control panel.
	- widl : Wine IDL compiler compiles (MS-RPC and DCOM) Interface
		Definition Language files.
	- wine :  The main Wine executable. This program will load a Windows
		binary and run it, relying upon the Wine shared object libraries.
	- wineboot : This program is executed on startup of the first wine
		process of a particular user.wineboot won't automatically run
		when needed.  Currently you have to manually run it after you
		install something.
	- winebuild : Winebuild is a tool used for building Winelib applications
		(and by Wine itself) to allow a developer to compile a .spec file
		into a .spec.c file.
	- wineconsole : Render the output of CUI programs.
	- winedbg : A application making use of the debugging API to allow
		debugging of Wine or Winelib applications as well as Wine itself
		(kernel and all DLLs).
	- winedump : Dumps the imports and exports of NE and PE files.
	- winefile : A clone of the win3x file manager.
	- winegcc/wineg++: Wrappers for gcc/g++ respectively, to make them behave
		as MinGW's gcc. Used for porting apps over to Winelib.
	- winemaker : Winemaker is a perl script which is designed to help you
		bootstrap the conversion of your Windows projects to Winelib.
	- winemine :  A clone of "Windows Minesweeper" a demo WineLib app.
	- winepath :  A tool for converting between Windows paths and Unix paths
	- wineserver : The Wine server is the process that manages resources,
		coordinates threads, and provides synchronization and interprocess
		communication primitives to Wine processes.
	- wineshelllink : This shell script can be called by Wine in order to
		propagate Desktop icon and menu creation requests out to a
		GNOME or KDE (or other Window Managers).
	- winewrap : Takes care of linking winelib applications. Linking with
		Winelib is a complex process, winewrap makes it simple.
	- winhelp : A Windows Help replacement.
	- wmc : Wine Message Compiler it allows Windows message files to be
		compiled into a format usable by Wine.
	- wrc : the Wine Resource Compiler. A clone of Microsoft's rc.

    * Shared Object Library Files
	To obtain a current list of DLLs, run:
		ls dlls/*.so
	it the root of the Wine _build_ tree, after a successful build.

    * Man Pages
	To obtain a current list of man files that need to be installed, run:
		find . -name "*.man"
	it the root of the Wine _build_ tree, after you have run ./configure.

    * Include Files
	An up to date list of includes can be found in the include/Makefile.in
	file.

    * Documentation files
	After building the documentation with:
		cd documentation; make html
	install all the files from: wine-user/, wine-devel/ and winelib-user/.

     * Dynamic Wine Files
        Wine also generates and depends on a number of dynamic
        files, including user configuration files and registry files.

        At the time of this writing, there was not a clear
        consensus of where these files should be located, and how
        they should be handled.  This section attempts
        to explain the alternatives clearly.


        - PREFIX/share/wine.inf
	
            This is the global Wine setup information file
	    in the format of a MS Installer .INF file.

    * Registry Files
            In order to replicate the Windows registry system,
            Wine stores registry entries in a series of files.

            For an excellent overview of this issue, read this
            http://www.winehq.org/News/2000-25.html#FTR
            Wine Weekly News feature.

            The bottom line is that, at Wine server startup,
            Wine loads all registry entries into memory
            to create an in memory image of the registry.
            The order of files which Wine uses to load
            registry entries is extremely important,
            as it affects what registry entries are
            actually present.  The order is roughly that
            .dat files from a Windows partition are loaded,
            and then finally local registry settings are
            loaded from WINEPREFIX.  As each set are loaded,
            they can override the prior entries.  Thus,
            the local registry files take precedence.

            Then, at exit (or at periodic intervals),
            Wine will write either all registry entries
            (or, with the default setting) changed
            registry entries to files in the WINEPREFIX.

	    - WINEPREFIX/system.reg
                This file contains the user's local copy of the
		HKEY_LOCAL_MACHINE registry hive.  In general use, it will
		contain only changes made to the default registry values.

	    - WINEPREFIX/user.reg
                This file contains the user's local copy of the
		HKEY_CURRENT_MACHINE registry hive.  In general use, it will
		contain only changes made to the default registry values.

	    - WINEPREFIX/userdef.reg
                This file contains the user's local copy of the
		HKEY_USERS\.Default registry hive.  In general use, it will
		contain only changes made to the default registry values.

	    - WINEPREFIX/cachedmetrics.[display]
                This file contains font metrics for the given X display.
                Generally, this cache is generated once at Wine start time.
                cachedmetrics can be generated if absent.
		You should note this can take a long time.

    * Important Files from a Windows Partition
        Wine has the ability to use files from an installation of the
        actual Microsoft Windows operating system.  Generally these
        files are loaded on a VFAT partition that is mounted under Linux.

        This is probably the most important configuration detail.
        The use of Windows registry and DLL files dramatically alters the
	behavior of Wine. If nothing else, packagers have to make this
	distinction clear to the end user, so that they can intelligently
        choose their configuration.

        - WINDOWSDIR/system32/system.dat
        - WINDOWSDIR/system32/user.dat
	- WINDOWSDIR/win.ini

    * Windows Dynamic Link Libraries (WINDOWSDIR/system32/*.dll)
        Wine has the ability to use the actual Windows DLL files
        when running an application.  An end user can configure
        Wine so that Wine uses some or all of these DLL files
        when running a given application.

PACKAGING STRATEGIES
~~~~~~~~~~~~~~~~~~~~

There has recently been a lot of discussion on the Wine development
mailing list about the best way to build Wine packages.

There was a lot of discussion, and several diverging points of view.
This section of the document attempts to present the areas of common
agreement, and also to present the different approaches advocated on
the mailing list.

    * Distribution of Wine into packages
        The most basic question to ask is given the Wine CVS tree,
        what physical files are you, the packager, going to produce?
        Are you going to produce only a wine.rpm, or are you going to
	produce 6 Debian files (libwine, libwine-dev, wine, wine-doc,
	wine-utils and winesetuptk) as Ove has done?
        At this point, common practice is to adopt to the conventions
	of the targeted distribution.

	Also, experience shows that you should not create a huge set
	of packages, since later upgrades and obsoleting will be
	painful.

    * Where to install files
        This question is not really contested.  It will vary
        by distribution, and is really up to the packager.
        As a guideline, the current 'make install' process
        seems to behave such that if we pick a single PREFIX then:
            - binary files go into PREFIX/bin
	    - library files go into PREFIX/lib/wine
	    - include files go into PREFIX/include/wine
	    - man pages go into PREFIX/share/man
	    - documentation files go into PREFIX/share/doc/wine-VERSION

	You might also want to use the wine wrapper script winelauncher
	that can be found in tools/ directory, as it has several important
	advantages over directly invoking the wine binary.
	See the Executable Files section for details.

    * The question of /opt/wine
        The FHS 2.2 specification suggests that Wine as a package
        should be installed to /opt/wine.  None of the existing packages
	follow this guideline (today; check again tomorrow).

	(Since most are upgrades of the distro packages, this is still
	 on the safe side I think - Marcus Meissner)

    * What files to create
        After installing the static and shareable files, the next
        question the packager needs to ask is how much dynamic
        configuration will be done, and what configuration
        files should be created.

        The best current approach to this is:
	- Leave it alone and make a "wineprefixcreate" call available
	  to the user via a menu item or similar.

        - Setup a fake windows setup automatically.
		
		This is done by simply calling wineprefixcreate,
		which will setup a fake windows root for the user.

		If no arguments are passed, defaults will be
		assumed for WINEPREFIX (~/.wine) and similar
		variables.

		After this, WINE is immediately usable by the
		end user.

	- Others might be possible.

IMPLEMENTATION
~~~~~~~~~~~~~~

This section discusses the implementation of a Red Hat 8.0 .spec file.
For a current .spec file, please refer to any one of the existing SRPMs.

1. Building the package

Wine is configured the usual way (depending on your build environment).
The PREFIX is chosen using your application placement policy
(/usr/, /usr/X11R6/, /opt/wine/, or similar).  The configuration files
(wine.userreg, wine.systemreg) are targeted for /etc/wine/
(rationale: FHS 2.2, multiple read only configuration files of a package).

Example (split this into %build and %install section for rpm:


	CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=/usr/X11R6 --sysconfdir=/etc/wine/ --enable-dll
	make
	BR=$RPM_BUILD_ROOT
	make install prefix=$BR/usr/X11R6/ sysconfdir=$BR/etc/wine/

You will need to package the files:

	$prefix/bin/wine*
	$prefix/lib/libwine*
	$prefix/lib/wine/*,
	$prefix/share/wine/wine.inf

	$prefix/man/man1/wine.1
	$prefix/include/wine/*
	$prefix/bin/notepad
	$prefix/bin/progman
	$prefix/bin/regedit
	$prefix/bin/rundll32
	$prefix/bin/regsvr32
	$prefix/bin/widl
	$prefix/bin/winhelp

	%config /etc/wine/*
	%doc ... choose from the top level directory and documentation/


2. Installing Wine for the system administrator

Install the package using the usual packager 'rpm -i wine.rpm'.

Adapting the $prefix/share/wine/wine.inf file used by wineprefixcreate is not
necessary.

Note that on Linux you should somehow try to add the unhide mount option
(see 'man mount') to the CD-ROM entry in /etc/fstab during package install,
as several stupid Windows programs mark some setup (!) files as hidden
(ISO9660) on CD-ROMs, which will greatly confuse users as they won't find
their setup files on the CD-ROMs as they were used on Windows systems when
unhide is not set ;-\ And of course the setup program will complain
that setup.ins or some other mess is missing... If you choose to do so,
then please make this change verbose to the admin.

Also make sure that the kernel you use includes the Joliet CD-ROM support,
for the very same reasons as given above (no long filenames due to missing
Joliet, files not found).

3. Installing Wine for the user

If no standard wine prefix was setup, the first call to wine will
create one for the user.

So the user can just click on any setup.exe file and it will work
out of the box.

AUTHORS
~~~~~~~

Written in 1999 by Marcus Meissner <marcus@jet.franken.de>
Updated in 2000 by Jeremy White <jwhite@codeweavers.com>
Updated in 2002 by Andreas Mohr <andi@rhlx01.fht-esslingen.de>
Updated in 2003 by Tom Wickline <twickline2@triad.rr.com>
Updated in 2003 by Dimitrie O. Paun <dpaun@rogers.com>
Updated in 2004,2005 by Marcus Meissner <marcus@jet.franken.de>
